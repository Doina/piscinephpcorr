<?php
class NightsWatch {
	public function __construct(){
		return;
	}

	private $fighter = array();

	public function recruit($param)
	{
		$this->fighter[] = $param;
	}
	public function fight()
	{
		foreach ($this->fighter as $value)
		{
			if ($value instanceof IFighter)
				$value->fight();
		}
	}

	public function __destruct(){
		return;
	}
}
?>
