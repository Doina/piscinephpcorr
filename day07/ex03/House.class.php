<?php
abstract class House {
	public function __construct(){
		return;
	}

	abstract public function getHouseName();

	abstract public function getHouseMotto();

	abstract public function getHouseSeat();

	function introduce() {
		echo "House ".$this->getHouseName()." of ".$this->getHouseSeat()." : " . '"'.$this->getHouseMotto().'"'."\n";
	}

	public function __destruct(){
		return;
	}
}
?>
