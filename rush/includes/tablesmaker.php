<?php

    function tabmake_articles($conn)
    {
    $urb = "CREATE TABLE articles(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(45) NOT NULL UNIQUE,
    price DOUBLE UNSIGNED NOT NULL,
    img_link VARCHAR(2000) NOT NULL )";

    $check = mysqli_query($conn, $urb) ? "Table articles created successfully\n" : "Error creating table: " . mysqli_error($conn)."\n";
    echo $check;
    ?><br><br><?php

    $stmt = mysqli_stmt_init($conn);
    mysqli_stmt_prepare($stmt,"INSERT INTO articles (id, name, price, img_link) VALUES (?, ?, ?, ?)");
    mysqli_stmt_bind_param($stmt, 'isis', $id, $name, $price, $img_link);


    $id = 1;
    $name = "Quiksilver - Lanai Sunset";
    $price = 80;
    $img_link = "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.blitzsurf.co.nz%2Fuser%2Fimages%2F11476.jpg%3Ft%3D1610171604&f=1";
    mysqli_stmt_execute($stmt);
    $id = 2;
    $name = "Quiksilver - Echo Beach";
    $price = 70;
    $img_link = "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.xtreme-skate.com%2Fskate-shop%2F1926-thickbox_default%2Fquiksilver-echo-beach-skateboard-cruiser-355-inch.jpg&f=1";
    mysqli_stmt_execute($stmt);
    $id = 3;
    $name = "Globe - Tracer";
    $price = 70;
    $img_link = "https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fimages.evo.com%2Fimgp%2F700%2F98560%2F421827%2Fglobe-tracer-classic-cruiser-complete-bamboo-black.jpg&f=1";
    mysqli_stmt_execute($stmt);
    $id = 4;
    $name = "Globe - Big Blazer";
    $price = 100;
    $img_link = "https://images.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.warehouseskateboards.com%2Fimages%2Fproducts%2Fpreview%2F1CGLB0BGBZ32BZR.jpg&f=1";
    mysqli_stmt_execute($stmt);

    $id = 5;
    $name = "Santa Cruz - Sugar Skull";
    $price = 140;
    $img_link = "https://planet-sports-res.cloudinary.com/images/q_80,f_auto,dpr_2.0,d_planetsports:products:nopic.jpg/planetsports/products/47100400_00/santa-cruz-sugar-skull-39-longboard-dores.jpg";
    mysqli_stmt_execute($stmt);
    $id = 6;
    $name = "Santa Cruz - Strip Inlay";
    $price = 120;
    $img_link = "http://www.chutingstar.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/s/a/santacruzcruzer_stripinlay.jpg";
    mysqli_stmt_execute($stmt);
    $id = 7;
    $name = "Jucker Hawaii - Makaha";
    $price = 150;
    $img_link = "https://www.juckerhawaii.fr/media/image/product/202/lg/jucker-hawaii-longboard-completes-makaha.jpg";
    mysqli_stmt_execute($stmt);
    $id = 8;
    $name = "Jucker Hawaii - Skaid";
    $price = 130;
    $img_link = "https://www.juckerhawaii.co.uk/media/image/product/356/md/jucker-hawaii-longboard-complete-skaid.jpg";
    mysqli_stmt_execute($stmt);

    $id = 9;
    $name = "Independent - Forged Hollow";
    $price = 58;
    $img_link = "https://www.basementskate.com.au/images/P/independent-139-matte-black-forged-hollow-skateboard-trucks.png";
    mysqli_stmt_execute($stmt);
    $id = 10;
    $name = "Independent - Hollow Mountain";
    $price = 65;
    $img_link = "https://www.basementskate.com.au/images/P/independent-mountin-hollow-raw-pewter-139-skateboard-trucks.png";
    mysqli_stmt_execute($stmt);
    $id = 11;
    $name = "Independent - Hollow 149";
    $price = 70;
    $img_link = "https://www.basementskate.com.au/images/P/independent-hollow-149-reynolds-gc-green-skateboard-trucks.png";
    mysqli_stmt_execute($stmt);
    $id = 12;
    $name = "Rogue - Black";
    $price = 79;
    $img_link = "https://www.muirskate.com/photos/products/5063/hd_product_Rogue-Cast-Trucks-Black-%28Set2-HD2%29.png";
    mysqli_stmt_execute($stmt);

        $id = 13;
        $name = "Radar - Varisty";
        $price = 30;
        $img_link = "https://www.skates.com/v/vspfiles/photos/RDR-VAR-BLK-2T.jpg";
        mysqli_stmt_execute($stmt);
        $id = 14;
        $name = "Fo-Mac - Free-Style";
        $price = 35;
        $img_link = "https://sohimages.com/images/images_skates/SGFMFSBL-2.jpg";
        mysqli_stmt_execute($stmt);
        $id = 15;
        $name = "Power - Roller Bones";
        $price = 25;
        $img_link = "https://cdn3.volusion.com/ehn32.tcm2q/v/vspfiles/photos/options/WRBARB26298-168.jpg?1487236157";
        mysqli_stmt_execute($stmt);
        $id = 16;
        $name = "Cadillac - White Walls";
        $price = 28;
        $img_link = "https://www.warehouseskateboards.com/images/products/preview/1WCAD0WHW5978KK.jpg";
        mysqli_stmt_execute($stmt);

    echo "Article records created successfully\n";
    mysqli_stmt_close($stmt);
    ?><br><br><?php
    }



    function tabmake_categories($conn)
    {
        $usr = "CREATE TABLE categories(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(45) NOT NULL UNIQUE,
img_link VARCHAR(2000) NOT NULL
)";

        $check = mysqli_query($conn, $usr) ? "Table categories created successfully\n" : "Error creating table: " . mysqli_error($conn)."\n";
        echo $check;

        ?><br><br><?php

        $stmt = mysqli_stmt_init($conn);
        mysqli_stmt_prepare($stmt,"INSERT INTO categories (id, name, img_link) VALUES (?, ?, ?)");
        mysqli_stmt_bind_param($stmt, 'iss', $id, $name, $img_link);

        $id = 1;
        $name = "Cruiser";
        $img_link = "https://www.basementskate.com.au/images/P/globe-the-acland-walnut-cruiser-skateboard-01.png";
        mysqli_stmt_execute($stmt);
        $id = 2;
        $name = "Longboard";
        $img_link = "https://www.decathlon.fr/media/834/8343004/big_4cf73c56-5ad8-4e10-b09e-b36b75204d6b.jpg";
        mysqli_stmt_execute($stmt);
        $id = 3;
        $name = "Truck";
        $img_link = "https://www.basementskate.com.au/images/P/independent-titanium-forged-black-trucks.png";
        mysqli_stmt_execute($stmt);
        $id = 4;
        $name = "Wheels";
        $img_link = "https://cdn.shopify.com/s/files/1/1496/9968/products/BonesATF59All.jpg?v=1518248660";
        mysqli_stmt_execute($stmt);

        echo "Longboards records created successfully\n";
        mysqli_stmt_close($stmt);

        ?><br><br><?php
    }


    function tabmake_usr($conn)
    {
        $usr = "CREATE TABLE users(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
login VARCHAR(30) NOT NULL UNIQUE, 
password VARCHAR(255) NOT NULL,
isAdm TINYINT DEFAULT 0)";


        $check = mysqli_query($conn, $usr) ? "Table users created successfully\n" : "Error creating table: " . mysqli_error($conn)."\n";
        echo $check;
        ?><br><br><?php

        $pass = hash ('whirlpool', mysqli_real_escape_string($conn, 'admin'));
        $query = "INSERT INTO users (login, password, isAdm) VALUES ('admin', '$pass', 1)";
        $res = mysqli_query($conn, $query);
        if (!$res)
            die("Admin user not created: " . mysqli_connect_error());
        echo "Admin users created\n";
        ?><br><br><?php
    }

    function tabmake_product_category($conn)
    {
        $pc = "CREATE TABLE IF NOT EXISTS product_category (
product_id int(11) NOT NULL, 
category_id int(11) NOT NULL, 
PRIMARY KEY  (product_id, category_id))";

        $check = mysqli_query($conn, $pc) ? "Table product_category created successfully\n" : "Error creating table: " . mysqli_error($conn)."\n";
        echo $check;
        ?><br><br><?php

        $query = "INSERT INTO product_category (product_id, category_id) VALUES (1, 1), (2, 1), (3, 1), (4, 1), (5, 2), (6, 2), (7, 2), (8, 2), (9, 3), (10, 3), (11, 3), (12, 3), (13, 4), (14, 4), (15, 4), (16, 4)";
        $res = mysqli_query($conn, $query);
        if (!$res)
            die("Error: " . mysqli_connect_error());
        ?><br><br><?php

    }


    function tabmake_orders($conn)
    {

        $ord = "CREATE TABLE orders(
id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
login VARCHAR(45) NOT NULL,
total_price DOUBLE UNSIGNED NOT NULL)";

        $check = mysqli_query($conn, $ord) ? "Table orders created successfully" : "Error creating table: " . mysqli_error($conn);
        ?><br><br><?php
        echo $check;
    }

    function tabmake_orders_items($conn)
    {
        $ord_itm = "CREATE TABLE orders_items(
id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
order_id int NOT NULL,
login VARCHAR(45) NOT NULL,
product_id int NOT NULL,
name VARCHAR(45) NOT NULL,
price DOUBLE UNSIGNED NOT NULL,
quantity INT UNSIGNED NOT NULL DEFAULT 1)";

        $check = mysqli_query($conn, $ord_itm) ? "Table orders items created successfully" : "Error creating table: " . mysqli_error($conn);
        ?><br><br><?php
        echo $check;
    }
    ?>