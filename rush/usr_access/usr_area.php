<!DOCTYPE html>

<html>

<head>
    <title>User Area</title>
    <link rel="stylesheet" href="../style.css">
</head>

<body>
<div class="user_area_header">
    <img class="logo" src="../img/logo.png">
    <div class="usr_sections">
        <a href="edit_acc/edit_pass.php"> Edit Account  |  </a>
        <a href="edit_acc/del_acc.php">Destroy Account </a>
    </div>

    <div class="home_sections" >
        <a href="../index.php" id="back_home">Back to Home</a>
    </div>
</div>

</body>

</html>