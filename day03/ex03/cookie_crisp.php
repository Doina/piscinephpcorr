<?php
function my_isset($str, $source )
{
	foreach($source as $key => $value)
		if ($str == $key)
			return (1);
	return (0);
}

if (my_isset('action', $_GET) && my_isset('name', $_GET))
{
	if ($_GET['action'] == 'set' && my_isset('value', $_GET))
			setcookie($_GET['name'], $_GET['value'], (time() + 3600));
	if ($_GET['action'] == 'del')
				setcookie($_GET['name'], null, (time() - 3600));
	if ($_GET['action'] == 'get' && my_isset($_GET['name'], $_COOKIE))
		echo $_COOKIE[$_GET['name']];
}
?>
