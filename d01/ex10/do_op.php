#!/usr/bin/php
<?php
if ($argc == 4)
{
	$new_array = array();
	foreach($argv as $k => $elem)
	{
		if ($k != 0)
			array_push($new_array, trim($elem));
	}
	if ($new_array[1] == '+')
		echo (intval($new_array[0]) + intval($new_array[2]))."\n";
	else if ($new_array[1] == '-')
		echo (intval($new_array[0]) - intval($new_array[2]))."\n";
	else if ($new_array[1] == '/' && $new_array[2] != 0)
		echo (intval($new_array[0]) / intval($new_array[2]))."\n";
	else if ($new_array[1] == '%' && $new_array[2] != 0)
		echo (intval($new_array[0]) % intval($new_array[2]))."\n";
	else if ($new_array[1] == '*')
		echo (intval($new_array[0]) * intval($new_array[2]))."\n";
}
?>
