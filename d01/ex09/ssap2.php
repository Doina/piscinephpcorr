#!/usr/bin/php
<?php
if ($argc >= 2)
{
function ft_pos($x)
{
	$arr = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
		"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1",
		"2", "3", "4", "5", "6", "7", "8", "9", " ", "!", "\"", "#", "$", "%",
		"&", "'", "(", ")", "*", "+", ",", "-", ".", "/", ":", ";", "<", "=", ">",
		"?", "@", "[", "\\", "]", "^", "_", "`", "{", "|", "}", "~");
	$pos = array_search($x, $arr);
		return ($pos);
}

function ft_strcmp($str1, $str2)
{
	$str1 = strtolower($str1);
	$str2 = strtolower($str2);
	$i = 0;
	while ($str1[$i] && $str2[$i] && $str1[$i] == $str2[$i])
		$i++;
	return (ft_pos($str1[$i]) - ft_pos($str2[$i]));
}

$new_array = array();
foreach($argv as $k => $elem)
{
	if ($k != 0)
	{
		$arr = array_filter(explode(' ', $elem));
		foreach($arr as $elem)
			array_push($new_array, $elem);
	}
}

usort($new_array, "ft_strcmp");
foreach ($new_array as $elem)
	echo "$elem\n";
}
?>
