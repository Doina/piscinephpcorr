#!/usr/bin/php
<?php
if ($argc >= 2)
{
	$new_array = array();
	$arr = array_filter(explode(' ', $argv[1]));
	foreach($arr as $k => $elem)
	{
		if ($k == 0)
			$aux = $elem;
		else
			array_push($new_array, $elem);
	}
	array_push($new_array, $aux);
	echo implode(" ", $new_array);
	echo "\n";
}
?>
